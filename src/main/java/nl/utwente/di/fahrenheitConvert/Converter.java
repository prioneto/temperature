package nl.utwente.di.fahrenheitConvert;

public class Converter {
    public double getBookPrice(String degrees) {
        return (Double.parseDouble(degrees) * 9/5) + 32;
    }
}
